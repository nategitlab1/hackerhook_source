#include "DPSE.h"
#include "Globals.h"
//#include "CGlowObjectManager.h"

namespace DPSE
{
	DoPostScreenEffects_t g_fnOriginalDoPostScreenEffects;
	int __stdcall Hooked_DPSE(int a1)
	{
		auto local = CBaseEntity::GetLocalPlayer();
	//	if (local && OptionsManager.bGlow)
	//		glow();
		return g_fnOriginalDoPostScreenEffects(SSDK::I::ClientMode(), a1);
	}
}