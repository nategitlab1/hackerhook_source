#include "Globals.h"

bool Globals::body_updated;

SSDK::QAngle Globals::fake_angles = SSDK::QAngle(0, 0, 0);
SSDK::QAngle Globals::real_angles = SSDK::QAngle(0, 0, 0);

int Globals::hit_shots;
int Globals::shots_fired;
int Globals::missed_shots;

float Globals::tick_base;

std::deque<AppLog> Globals::events;