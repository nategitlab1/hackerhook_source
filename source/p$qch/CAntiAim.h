#pragma once

#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"

#include "Utilities.h"
#include "Globals.h"
#include "CAutoWall.h"

enum aa_manual_sides
{
	aa_left = 0,
	aa_back = 1,
	aa_right = 2
};

class CAntiaim
{
public:
	void run_aa(SSDK::CUserCmd* cmd, CBaseEntity* local, bool &bSendPacket);
private:
	float do_real(SSDK::CUserCmd* cmd, CBaseEntity* local);
	float do_moving_real(SSDK::CUserCmd* cmd, CBaseEntity* local);
	float do_fake(SSDK::CUserCmd* cmd, CBaseEntity* local);
	float do_pitch(SSDK::CUserCmd* cmd,CBaseEntity* local);
};