#include "SceneEnd.h"

namespace SceneEnd
{
	static float vis_col[3] = { 107.f / 255.f, 169.f / 255.f, 233.f / 255.f };
	static float hid_col[3] = { 130.f / 255.f, 96.f / 255.f, 224.f / 255.f };
	static float gos_col[3] = { 1.f, 1.f, 1.f };

	SceneEnd_t g_fnOriginalSceneEnd;
	void __stdcall Hooked_SceneEnd()
	{
		g_fnOriginalSceneEnd(SSDK::I::RenderView());

		auto local = CBaseEntity::GetLocalPlayer();

		SSDK::IMaterial *material =
			(OptionsManager.bChamsmode < 1) ?
			SSDK::I::MatSystem()->FindMaterial("debug/debugdrawflat", TEXTURE_GROUP_MODEL) :
			SSDK::I::MatSystem()->FindMaterial("chams", TEXTURE_GROUP_MODEL);

		for (int i = 1; i <= SSDK::I::GlobalVars()->maxClients; ++i) {
			auto ent = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(i);

			if (ent && ent->isValidPlayer())
			{
				if (material)
				{
					SSDK::I::RenderView()->SetColorModulation(hid_col);
					material->IncrementReferenceCount();
					material->SetMaterialVarFlag(SSDK::MATERIAL_VAR_IGNOREZ, true);
					SSDK::I::ModelRender()->ForcedMaterialOverride(material);
					ent->DrawModel(0x1, 255);
					SSDK::I::ModelRender()->ForcedMaterialOverride(nullptr);

					SSDK::I::RenderView()->SetColorModulation(hid_col);
					material->IncrementReferenceCount();
					material->SetMaterialVarFlag(SSDK::MATERIAL_VAR_IGNOREZ, false);
					SSDK::I::ModelRender()->ForcedMaterialOverride(material);
					ent->DrawModel(0x1, 255);
					SSDK::I::ModelRender()->ForcedMaterialOverride(nullptr);
				}
			}
		}

		// below is fucking disgusting, don't do this, do it properly so u dont fuck ur legs.
		// add glow, alpha, scoped in alpha of player, custom colors, work on resolvo, work on antiaims, change menu look
		if (local && local->IsAlive() && SSDK::I::Input()->m_fCameraInThirdPerson == true && OptionsManager.iIndicators == 1 || OptionsManager.iIndicators == 3)
		{
			material = SSDK::I::MatSystem()->FindMaterial(XorStr("chams"), TEXTURE_GROUP_MODEL);
			if (material)
			{
				local->SetAngle(SSDK::Vector(0, Globals::fake_angles.y, 0));
				SSDK::I::RenderView()->SetColorModulation(gos_col);
				SSDK::I::RenderView()->SetBlend(.8f);
				SSDK::I::ModelRender()->ForcedMaterialOverride(material);
				local->DrawModel(0x1, 255);
				SSDK::I::ModelRender()->ForcedMaterialOverride(nullptr);
				local->SetAngle(Globals::real_angles);
			}
		}
	}
}