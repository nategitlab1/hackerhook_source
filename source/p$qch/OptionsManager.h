#pragma once

#include "EntityStructs.h"

class COptions {
public:
	//Global
	bool bAutoHop = false;

	//Menu related
	bool bShowMenu = false;
	bool vecPressedKeys[256] = {};

	//VISUALS
	bool bEspEnabled = false;
	bool bBoxESP = false;
	bool bHealthbar = false;
	bool bPlayerName = false;
	bool bGlow = false;
	bool bGlowEnemyOnly = false;
	bool bGlowPlayer = false;
	bool bNoScope = false;
	bool bChams = false;
	int bChamsmode = 0;
	bool bChamsEnemyOnly = false;
	bool bChamsPlayer = false;
	bool bBones = false;
	bool bCrosshair = false;
	bool bBulletTracers = false;

	//AIMBOT
	bool bAutoShoot = true;
	bool bAutoPistol = true;
	bool bAutoRevolver = true;
	bool bAutoScope = true;
	bool lbyone = true;
	bool bSilentAim = true;
	int iHitchance = 0;
	int iHitChanceSeed = 256;
	int iPointScale = 74;
	

	bool bCorrect = true;
	int iResolverType = 0;
	bool aMultiHitboxes[15] = {
		true, true, true, true, true,
		true, true, true, true, true,
		true, true, true, true, true
	};
	bool bPrioritize = true;
	int iHitbox = 0;
	bool bAutoBaim = false;
	int iAutoBaimAferShot = 3;
	int iHitboxAutoBaim = 4;
	bool bPrioritizeVis = false;
	float flMinDmg = 5.f;
	bool bInterpLagComp = true;

	//ANTI-AIM
	bool bFakeLag = false;
	int iFakeLagAmount = 0;
	bool bAA = true;
	bool iAAPitch = false;
	bool iAARealYaw = false;
	bool iAAFakeYaw = false;
	int iAAMoveRealYaw = 0;
	int iBreakerDelta = 0;
	bool bFakeWalk = false;
	bool bManualSide = false;
	bool bManualJitter = false;
	int iManualJitterRange = 0;
	float speed_in_air = 130.f;
	float reset_in_air_range = 145.f;

	// Misc
	bool bNoVisRecoil = false;
	bool bNoFlash = false;
	bool bClantagchanger = false;
	bool bWaterMark = false;
	int iIndicators = 0;
	int iFov = 90;
	bool iThirdPerson = false;
	bool bEventLog = false;
};

extern COptions OptionsManager;