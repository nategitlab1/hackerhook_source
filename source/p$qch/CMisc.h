#pragma once

#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"

#include "Utilities.h"
#include "Globals.h"
#include <iostream>
#include <cstdlib>
#include <cmath>
#include <iomanip>

namespace MiscCheats
{
	void Strafer(SSDK::CUserCmd* cmd, CBaseEntity* local);
	void Bunnyhop(SSDK::CUserCmd* cmd, CBaseEntity* local);
	void FakeLag(SSDK::CUserCmd* cmd, CBaseEntity* local);
}