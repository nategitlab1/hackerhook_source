#pragma once

#include "SqchSDK\Interfaces.h"
#include "EntityStructs.h"

namespace DPSE
{
	typedef int(__thiscall* DoPostScreenEffects_t)(SSDK::IClientMode*, int);

	extern DoPostScreenEffects_t g_fnOriginalDoPostScreenEffects;

	int __stdcall Hooked_DPSE(int a1);
}