#include <Windows.h>
#include "SqchSDK\Interfaces.h"
#include "SqchSDK\Offsets.h"

#include "EntityStructs.h"
#include "OptionsManager.h"
#include "Globals.h"

#include "Utilities.h"
#include "LogManager.h"
#pragma comment(lib, "Winmm.lib")

class CHurtListener
{
	class HurtListener : public IGameEventListener2
	{
	public:
		void start()
		{
			if (!SSDK::I::GameEvents()->AddListener(this, "player_hurt", false))
			{
				SSDK::I::CVar()->ConsoleColorPrintf(SSDK::Color(255, 0, 0), XorStr("Failed to register player hurt listener. \n"));
			}
		}
		void stop()
		{
			SSDK::I::GameEvents()->RemoveListener(this);
		}
		void FireGameEvent(IGameEvent* event) override
		{
			CHurtListener::singleton()->OnFireEvent(event);
		}
		int GetEventDebugID(void) override
		{
			return EVENT_DEBUG_ID_INIT /*0x2A*/;
		}
	};
public:
	static CHurtListener* singleton()
	{
		static CHurtListener* instance = new CHurtListener;
		return instance;
	}

	void init()
	{
		_listener.start();
	}

	void OnFireEvent(IGameEvent* event)
	{
		if (!strcmp(event->GetName(), "player_hurt"))
		{
			int attacker = event->GetInt("attacker");
			int playerid = event->GetInt("userid");
			int damage = event->GetInt("dmg_health");
			int hitgroup = event->GetInt("hitgroup");

			SSDK::PlayerInfo entity_info;
			auto entity_index = SSDK::I::Engine()->GetPlayerUserID(playerid);
			CBaseEntity* entity = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(entity_index);

			if (SSDK::I::Engine()->GetPlayerUserID(attacker) == SSDK::I::Engine()->GetLocalPlayer())
			{
				// player hurt called = true;

				/* ~~ Log hits for resolving ~~ */

				Globals::hit_shots++;

				Globals::missed_shots = 0;

				/* ~~ End of Logging hits ~~*/


				/* ~~ Increase Hurt Timer ~~ */
				_flHurtTime = SSDK::I::GlobalVars()->curtime;
				_hitmarkeralpha = 1.f;

				auto curtime = SSDK::I::GlobalVars()->curtime;

				/* ~~ Play Skeet Sound ~~ */
			//	if (_flHurtTime + 0.25f >= curtime)
				SSDK::I::Engine()->ClientCmd_Unrestricted("play buttons\\arena_switch_press_02.wav");
			}
		}
	}

	void OnPaint()
	{
		auto curtime = SSDK::I::GlobalVars()->curtime;
		auto lineSize = 10;

		if (_hitmarkeralpha < 0.f)
			_hitmarkeralpha = 0.f;
		else if (_hitmarkeralpha > 0.f)
			_hitmarkeralpha -= 0.01f;

		int screenSizeX, screenCenterX;
		int screenSizeY, screenCenterY;

		SSDK::I::Engine()->GetScreenSize(screenSizeX, screenSizeY);

		screenCenterX = screenSizeX / 2;
		screenCenterY = screenSizeY / 2;

		if (_hitmarkeralpha > 0.f)
		{
			SSDK::I::MatSurface()->DrawSetColor(SSDK::Color(230, 230, 230, (_hitmarkeralpha * 255)));
			SSDK::I::MatSurface()->DrawLine(screenCenterX - lineSize, screenCenterY - lineSize, screenCenterX - (lineSize / 2), screenCenterY - (lineSize / 2));
			SSDK::I::MatSurface()->DrawLine(screenCenterX - lineSize, screenCenterY + lineSize, screenCenterX - (lineSize / 2), screenCenterY + (lineSize / 2));
			SSDK::I::MatSurface()->DrawLine(screenCenterX + lineSize, screenCenterY + lineSize, screenCenterX + (lineSize / 2), screenCenterY + (lineSize / 2));
			SSDK::I::MatSurface()->DrawLine(screenCenterX + lineSize, screenCenterY - lineSize, screenCenterX + (lineSize / 2), screenCenterY - (lineSize / 2));
		}

		for (size_t i = 0; i < Globals::events.size(); i++)
		{
			if (!OptionsManager.bEventLog)
				return;

			float time = SSDK::I::GlobalVars()->curtime - Globals::events[i].time;

			DrawManager::DrawString(Globals::events[i].color, DrawManager::hFontESP, Globals::events[i].text.c_str(), 25, 150.f - i * 20);
		}
	}

private:
	HurtListener _listener;
	float        _flHurtTime;
	float        _hitmarkeralpha;
};