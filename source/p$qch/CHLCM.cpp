#include "CHLCM.h"

#include "CLagCompensation.h"
#include <time.h>
#include "DrawManager.h"
#include "Globals.h"
#include "CMisc.h"
#include "CAntiAim.h"

SSDK::QAngle CreateMoveAngles = SSDK::QAngle( 0, 0, 0 );

#define RANDOM_FLOAT( min, max ) (( min + 1 ) + ( ( ( float ) rand( ) ) / ( float ) RAND_MAX ) * ( max - ( min + 1 ) ))

namespace CHLCM {
	static CFixMove *FixMoveManager = new CFixMove( );
	static CPredictionSystem *Pred = new CPredictionSystem( );
	static CAntiaim* Antiaim = new CAntiaim( );

	CreateMoveCHL_t g_fnOriginalCreateMoveCHL;
	void __stdcall Hooked_CreateMove_CHL( int sequence_number, float input_sample_frametime, bool active, bool& bSendPacket ) {
		g_fnOriginalCreateMoveCHL( ( void* ) SSDK::I::Client( ), sequence_number, input_sample_frametime, active );

		if ( SSDK::I::Engine( )->isInGame( ) && SSDK::I::Engine( )->isConnected( ) ) {

			CBaseEntity *local = CBaseEntity::GetLocalPlayer( );

			if ( !local && !local->IsAlive( ) )
				return;

			SSDK::CVerifiedUserCmd* pVerif = &( *( SSDK::CVerifiedUserCmd** )( ( DWORD ) SSDK::I::Input( ) + 0xF0 ) )[ sequence_number % 150 ];
			SSDK::CUserCmd *cmd = &( *( SSDK::CUserCmd** )( ( DWORD ) SSDK::I::Input( ) + 0xEC ) )[ sequence_number % 150 ];

			if ( cmd && pVerif ) {

				Globals::tick_base = Utilities::calc_tickbase( cmd, local );

				MiscCheats::Bunnyhop( cmd, local );
			//	MiscCheats::Strafer( cmd, local );

				// aimboattttt (also a pretty nice one imho just messy)
				Pred->StartPrediction( cmd );
				FixMoveManager->Start( cmd );

				float cur_time = SSDK::I::GlobalVars( )->curtime;
				CBaseCombatWeapon *activeWeapon = local->GetActiveWeapon( );

				for ( int i = 1; i <= SSDK::I::Engine( )->GetMaxClients( ); ++i ) {

					CBaseEntity *target = ( CBaseEntity* ) SSDK::I::EntityList( )->GetClientEntity( i );

					if ( target->isValidPlayer( ) ) {
						if ( *local->GetTeamNum( ) != *target->GetTeamNum( ) ) {

							int hitbox = 0;
							int baim_hitbox = 6;

							if ( OptionsManager.iHitbox == 0 )
							{
								hitbox = ( int ) SSDK::HITBOX_HEAD;
							}
							else if ( OptionsManager.iHitbox == 1 )
							{
								hitbox = ( int ) SSDK::HITBOX_NECK;
							}
							else if ( OptionsManager.iHitbox == 2 )
							{
								hitbox = ( int ) SSDK::HITBOX_CHEST;
							}
							else if ( OptionsManager.iHitbox == 3 )
							{
								hitbox = ( int ) SSDK::HITBOX_PELVIS;
							}

							if ( OptionsManager.iHitboxAutoBaim == 0 )
							{
								baim_hitbox = ( int ) SSDK::HITBOX_CHEST;
							}
							else if ( OptionsManager.iHitboxAutoBaim == 1 )
							{
								baim_hitbox = ( int ) SSDK::HITBOX_BODY;
							}
							else if ( OptionsManager.iHitboxAutoBaim == 2 )
							{
								baim_hitbox = ( int ) SSDK::PELVIS;
							}

							if ( OptionsManager.bInterpLagComp )
								StartLagCompensation( target, cmd );

							SSDK::Vector vecClientTarget;

							if ( Globals::shots_fired > OptionsManager.iAutoBaimAferShot && OptionsManager.bAutoBaim )
							{
								vecClientTarget = CAutowall::CalculateBestPoint( target, baim_hitbox, OptionsManager.flMinDmg, OptionsManager.bPrioritize );
							}
							else
							{
								vecClientTarget = CAutowall::CalculateBestPoint( target, hitbox, OptionsManager.flMinDmg, OptionsManager.bPrioritize );
							}

							if ( vecClientTarget.IsValid( ) ) {
								SSDK::QAngle qaNewViewAngle = Utilities::CalcAngle( local->GetEyePos( ), vecClientTarget ) - *local->AimPunch( ) * 2.f;

								if ( Utilities::ClampAll( qaNewViewAngle ) && activeWeapon->NextPrimaryAttack( ) <= cur_time ) {

									cmd->viewangles = qaNewViewAngle;

									if ( activeWeapon && activeWeapon->isValidWeapon( ) ) {
										if ( activeWeapon->NextPrimaryAttack( ) <= cur_time ) {
											if ( !local->isScoped( ) && activeWeapon->isWeaponScope( ) ) {
												if ( OptionsManager.bAutoScope )
													cmd->buttons |= IN_ATTACK2;
											}
											else if ( Utilities::HitChance( local, target, activeWeapon, qaNewViewAngle, cmd->random_seed, OptionsManager.iHitchance ) /*Utilities::HitChanceBigPasta(local, pClient, cmd, activeWeapon)*/ ) {
												if ( OptionsManager.bAutoShoot ) {
													if ( activeWeapon->GetCurrentAmmo( ) )
													{
														cmd->buttons |= IN_ATTACK;
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
				
				// PERFECT FUCKING pSILENT BOY$ I PASTE OFF UNKNOWNCHEATS UNTOUCH HOLY SHIT DUDE IM A GOD
				if ( OptionsManager.bSilentAim ) {
					if ( cmd->buttons & IN_ATTACK ) {
						bSendPacket = false;
					}
					else {
						bSendPacket = true;
						cmd->viewangles = FixMoveManager->GetOldAngle( );
					}
				}

				FixMoveManager->Stop( cmd );
				Pred->EndPrediction( );

				// fakelag compensation break? nah, more like static 14 packet choke if moving lul
				static int choked;
				if ( OptionsManager.bFakeLag )
				{
					OptionsManager.iFakeLagAmount = Utilities::FakelagCompensationBreak( );

					if ( choked < OptionsManager.iFakeLagAmount && !( cmd->buttons & IN_ATTACK ) )
					{
						bSendPacket = false;
						choked++;
					}
					else
					{
						bSendPacket = true;
						choked = 0;
					}
				}

				// AA
				if ( OptionsManager.bAA )
				{
					if ( activeWeapon )
					{
						FixMoveManager->Start( cmd );

						Antiaim->run_aa( cmd, local, bSendPacket );

						FixMoveManager->Stop( cmd );
					}
				}

				Globals::real_angles = CreateMoveAngles;

				pVerif->m_cmd = *cmd;
				pVerif->m_crc = cmd->GetChecksum( );
			}
			if ( !bSendPacket )
				CreateMoveAngles = cmd->viewangles;
			else if ( bSendPacket )
				Globals::fake_angles = cmd->viewangles;
		}
	}
}

#undef RANDOM_FLOAT