#include "CAntiaim.h"

// holy fuck genius idea random_float(random_float(2, 4), randomfloat u get the point

// rewrite after bones (uc this is a good tv show? ok?)


// do a better engine prediction instead of a start stop.... like what are we, fucking retarded? (also fix move, just do it all at end of cm)
float server_time( CBaseEntity* local )
{
	return ( float ) *local->GetTickBase( ) * SSDK::I::GlobalVars( )->interval_per_tick;
}

bool ready_to_attack( CBaseEntity* local )
{
	if ( !local->GetActiveWeapon( ) )
		return false;

	float next_attack = local->GetActiveWeapon( )->NextPrimaryAttack( );

	return next_attack <= Globals::tick_base;
}

bool weapon_checks( SSDK::CUserCmd* cmd, CBaseEntity* local )
{
	if ( cmd->buttons & IN_ATTACK || cmd->buttons & IN_ATTACK2 && !( local->GetActiveWeapon( )->isGrenade( ) && local->GetActiveWeapon( ) ) )
	{
		int local_weapon = local->GetActiveWeapon( )->GetId( );

		if ( local_weapon == SSDK::weapon_knife )
			return true;

		else if ( local_weapon == SSDK::weapon_revolver )
		{
			if ( cmd->buttons & IN_ATTACK2 )
				return true;

			else
			{
				SSDK::INetChannelInfo* nci = SSDK::I::Engine( )->GetNetChannelInfo( );
				float latency = nci->GetAvgLatency( 0 );

				if ( !( local->GetActiveWeapon( )->PostponeFireReady( ) - ( Globals::tick_base - latency ) > 0.03f ) )
					return true;
			}
		}

		else if ( !( cmd->buttons & IN_ATTACK2 ) )
			return true;
	}

	else if ( local->GetActiveWeapon( ) && local->GetActiveWeapon( )->isGrenade( ) )
	{
		if ( local->GetActiveWeapon( )->getGrenade( )->GetThrowTime( ) > 0.f )
			return true;
	}

	return false;
}

// this is not how u count ticks btw. don't follow this shit example, was testing
void fakewalk( SSDK::CUserCmd* cmd, CBaseEntity* local, bool &bSendPacket )
{
	static int choked = -1;

	if ( GetAsyncKeyState( VK_SHIFT ) & 0x8000 && OptionsManager.bFakeWalk )
	{
		if ( local && local->IsAlive( ) )
		{
			choked++;

			if ( choked < 11 )
			{
				bSendPacket = false;
			}
			else if ( choked <= 13 )
			{
				bSendPacket = false;

				SSDK::Vector velocity = *local->GetVelocity( );
				SSDK::QAngle direction;
				Utilities::VectorAngles( velocity, direction );
				float speed = velocity.Length( );

				direction.y = cmd->viewangles.y - direction.y;

				SSDK::Vector negated_direction = direction.Forward( ) * -speed;

				cmd->forwardmove = negated_direction.x;
				cmd->sidemove = negated_direction.y;
			}
			else if ( choked >= 14 )
			{
				bSendPacket = true;

				choked = -1;
			}
		}
	}
}

enum manual_sides
{
	left = 0,
	right,
	back,
	none
};

bool do_manual( float yaw, bool &bSendPacket, CBaseEntity* local )
{
	bool is_fake = bSendPacket ? true : false;

	static manual_sides manual_mode = none;

	if ( GetAsyncKeyState( VK_LEFT ) )
	{
		manual_mode = left;
	}
	else if ( GetAsyncKeyState( VK_RIGHT ) )
	{
		manual_mode = right;
	}
	else if ( GetAsyncKeyState( VK_DOWN ) )
	{
		manual_mode = back;
	}

	if ( manual_mode == none )
	{
		return false;
	}

	switch ( manual_mode )
	{
	case left:
	{
		if ( is_fake )
		{
			yaw = OptionsManager.bManualJitter ? -90.f + Utilities::random_float( -OptionsManager.iManualJitterRange, OptionsManager.iManualJitterRange ) : -90.f; /* todo: random float */
			break;
		}

		yaw = OptionsManager.bManualJitter ? 90.f + Utilities::random_float( -OptionsManager.iManualJitterRange, OptionsManager.iManualJitterRange ) : 90.f;
		break;
	}
	case right:
	{
		if ( is_fake )
		{
			yaw = OptionsManager.bManualJitter ? 90.f + Utilities::random_float( -OptionsManager.iManualJitterRange, OptionsManager.iManualJitterRange ) : 90.f;
			break;
		}

		yaw = OptionsManager.bManualJitter ? -90.f + Utilities::random_float( -OptionsManager.iManualJitterRange, OptionsManager.iManualJitterRange ) : -90.f;
		break;
	}
	case back:
	{
		if ( is_fake )
		{
			static auto inverse = false;

			if ( ( ( *local->GetTickBase( ) ) % 3 ) == 0 )
			{
				inverse = !inverse;
			}

			auto angle = ( ( ( *local->GetTickBase( ) ) % 3 ) == 2 ) && !( ( ( *local->GetTickBase( ) ) % 3 ) == 0 );
			auto add = angle ? 90 : -90.0;

			yaw = inverse ? ( add ) : -( add );
			break;
		}

		yaw = OptionsManager.bManualJitter ? -180.f + Utilities::random_float( -OptionsManager.iManualJitterRange, OptionsManager.iManualJitterRange ) : -180.f;
		break;
	}
	}

	return true;
}

float CAntiaim::do_pitch(SSDK::CUserCmd* cmd, CBaseEntity* local)
{
	SSDK::AnimationLayer anim[13];



	if (OptionsManager.iAAPitch)
	{
	return  89.99;
	}
}

float CAntiaim::do_fake( SSDK::CUserCmd* cmd, CBaseEntity* local ) // fake?
{
	bool in_fakewalk = OptionsManager.bFakeWalk && GetAsyncKeyState( VK_SHIFT ) & 0x8000;

	if ( OptionsManager.iAAFakeYaw )
	{
		// 90 switch
		static auto inverse = false;

		if ( ( ( *local->GetTickBase( ) ) % 3 ) == 0 )
		{
			inverse = !inverse;
		}

		auto angle = ( ( ( *local->GetTickBase( ) ) % 3 ) == 2 ) && !( ( ( *local->GetTickBase( ) ) % 3 ) == 0 );
		auto add = angle ? 90 : -90.0;

		return inverse ? ( add ) : -( add );
	}

}

float CAntiaim::do_real( SSDK::CUserCmd* cmd, CBaseEntity* local ) // standing / air
{
	if ( OptionsManager.iAARealYaw )
	{

		auto local_on_ground = (*local->GetFlags() & (int)SSDK::EntityFlags::FL_ONGROUND);

		if (!local_on_ground)
		{
			auto air_value = fmod(SSDK::I::GlobalVars()->curtime / 0.9f * OptionsManager.speed_in_air, OptionsManager.reset_in_air_range);
			return + 180 -(OptionsManager.reset_in_air_range / 2) + air_value;
		}
		else
		{
			auto yaw = fmod(SSDK::I::GlobalVars()->curtime * (120.0f + Utilities::random_float(-17.45f, 17.45f)), 70.0f);
			return yaw + 145.0f;
		}

	



	}
}

float CAntiaim::do_moving_real( SSDK::CUserCmd* cmd, CBaseEntity* local ) // moving on ground
{
	
		auto yaw_value = fmodf((*local->GetTickBase() * SSDK::I::GlobalVars()->interval_per_tick) * 200.0f, 75.0f);
		return yaw_value + 145.0f;
		

}

void CAntiaim::run_aa( SSDK::CUserCmd* cmd, CBaseEntity* local, bool &bSendPacket )
{
	bool in_fakewalk = OptionsManager.bFakeWalk && GetAsyncKeyState( VK_SHIFT ) & 0x8000;

	// c00l meme dude
	if ( weapon_checks( cmd, local ) )
		return;

	if ( !OptionsManager.bFakeLag )
		bSendPacket = ( cmd->command_number % 3 == 0 ) ? true : false;

	// wasn't finished, was just starting fake walk but yeah.
	if ( OptionsManager.bFakeWalk )
		fakewalk( cmd, local, bSendPacket );

	// fuck u
	if ( local->GetMoveType( ) == ( int ) SSDK::MOVETYPE_LADDER || local->GetMoveType( ) == ( int ) SSDK::MOVETYPE_NOCLIP )
		return;

	SSDK::QAngle final_angle = cmd->viewangles;
	static float yaw;

	// untested lol fuck this cheat ;))))
	if ( do_manual( yaw, bSendPacket, local ) && OptionsManager.bManualSide )
	{
		/* cool man */
	}
	else // not a mess, just highly sophisticated coding style
	{
		if ( local->GetVelocity( )->Length2D( ) > 0.1 && *local->GetFlags( ) & ( int ) SSDK::EntityFlags::FL_ONGROUND && !in_fakewalk )
		{
			if ( bSendPacket )
			{
				yaw = do_fake( cmd, local );
			}
			else
			{
				yaw = do_moving_real( cmd, local );
			}
		}
		else
		{
			if ( bSendPacket )
			{
				yaw = do_fake( cmd, local );
			}
			else if ( !bSendPacket || Globals::body_updated )
			{
				if ( Globals::body_updated )
				{
					yaw = do_real( cmd, local ) + OptionsManager.iBreakerDelta;
				}
				else
				{
					yaw = do_real( cmd, local );
				}
			}
		}
	}

	if ( yaw )
	{
		final_angle.y += yaw;
	}

	// so this is to make ur z value above 999, don't touch because it's god codenz
	final_angle.x = do_pitch( cmd, local);

	if ( Utilities::ClampAll( final_angle ) )
	{
		cmd->viewangles = final_angle;
	}




}