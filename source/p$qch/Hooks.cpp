#include "Hooks.h"

#include "SqchSDK\Interfaces.h"
#include "CHurtListener.h"
#include "CRSListener.h"
#include "CBulletListener.h"
#include "BeginFrame.h"
#include "DPSE.h"
#include "SceneEnd.h"
#include "CFireListener.h"

namespace Hooks {

	// lol no more shitty sqchhook hooking with ur c casts i make it superior
	std::unique_ptr<VTHook::Hook> client = nullptr;
	std::unique_ptr<VTHook::Hook> client_mode = nullptr;
	std::unique_ptr<VTHook::Hook> engine = nullptr;
	std::unique_ptr<VTHook::Hook> studio_render = nullptr;
	std::unique_ptr<VTHook::Hook> model_render = nullptr;
	std::unique_ptr<VTHook::Hook> vgui = nullptr;
	std::unique_ptr<VTHook::Hook> d3d_device9 = nullptr;
	std::unique_ptr<VTHook::Hook> render_view = nullptr;

	void __declspec( naked ) __stdcall HookedCreateMoveNaked( int sequence_number, float input_sample_frametime, bool active )
	{
		__asm
		{
			push ebp
			mov  ebp, esp
			push ebx
			lea  ecx, [ esp ]
			push ecx
			push dword ptr[ active ]
			push dword ptr[ input_sample_frametime ]
			push dword ptr[ sequence_number ]
			call CHLCM::Hooked_CreateMove_CHL
			pop  ebx
			pop  ebp
			retn 0Ch
		}
	}

	typedef void( __thiscall* ClipRay )( void*, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::IHandleEntity *pEnt, SSDK::CGameTrace *pTrace );
	ClipRay g_fnOriginalClipRay;
	void __fastcall Hooked_ClipRay( void* thisptr, void* edx, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::IHandleEntity *pEnt, SSDK::CGameTrace *pTrace ) {
		g_fnOriginalClipRay( thisptr, ray, fMask, pEnt, pTrace );
		pTrace->fraction = 1.f;
	}

	typedef void( __thiscall* TraceRay )( void*, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::ITraceFilter *pTraceFilter, SSDK::CGameTrace *pTrace );
	TraceRay g_fnOriginalTraceRay;
	void __fastcall Hooked_TracRay( void* thisptr, void* edx, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::ITraceFilter *pTraceFilter, SSDK::CGameTrace *pTrace ) {
		g_fnOriginalTraceRay( thisptr, ray, fMask, pTraceFilter, pTrace );
		pTrace->fraction = 1.f;
	}

	typedef void( __thiscall* RayCollide )( void*, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::ICollideable *pCollide, SSDK::CGameTrace *pTrace );
	RayCollide g_fnOriginalRayCollide;
	void __fastcall Hooked_RayCollide( void* thisptr, void* edx, const SSDK::Ray_t &ray, unsigned int fMask, SSDK::ICollideable *pCollide, SSDK::CGameTrace *pTrace ) {
		g_fnOriginalRayCollide( thisptr, ray, fMask, pCollide, pTrace );
		pTrace->fraction = 1.f;
	}

	// so this is what u call when u want to uninject the codenz
	void Initialize( ) {
		//-----------------------------------------------------------------------------------------------

		NetvarManager::Instance( )->CreateDatabase( );

		DrawManager::InitializeFont( XorStr( "Verdana" ), 15 );

		DM::InitChams( ); // setup the s1ck chams boy

		//-----------------------------------------------------------------------------------------------

		auto dwDevice = **( uint32_t** ) ( SSDK::O::FindSignature( XorStr( "shaderapidx9.dll" ), XorStr( "A1 ? ? ? ? 50 8B 08 FF 51 0C" ) ) + 1 );

		//-----------------------------------------------------------------------------------------------

		client = std::make_unique<VTHook::Hook>( SSDK::I::Client( ) );
		engine = std::make_unique<VTHook::Hook>( SSDK::I::Engine( ) );
		client_mode = std::make_unique<VTHook::Hook>( SSDK::I::ClientMode( ) );
		model_render = std::make_unique<VTHook::Hook>( SSDK::I::ModelRender( ) );
		studio_render = std::make_unique<VTHook::Hook>( SSDK::I::StudioRender( ) );
		vgui = std::make_unique<VTHook::Hook>( SSDK::I::VGUIPanel( ) );
		d3d_device9 = std::make_unique<VTHook::Hook>( reinterpret_cast< void* >( dwDevice ) );
		render_view = std::make_unique<VTHook::Hook>( SSDK::I::RenderView( ) );

		while ( !( DirectX::g_hWindow = FindWindowA( XorStr( "Valve001" ), NULL ) ) ) Sleep( 200 );

		if ( DirectX::g_hWindow )
			DirectX::g_pOldWindowProc = ( WNDPROC ) SetWindowLongPtr( DirectX::g_hWindow, GWLP_WNDPROC, ( LONG_PTR ) DirectX::Hooked_WndProc );

		//Drawing hooks
		PT::g_fnOriginalPaintTraverse = vgui->HookMethod<PT::PaintTraverse_t>( PT::Hooked_PaintTraverse, 41 );
		DirectX::g_fnOriginalReset = d3d_device9->HookMethod<DirectX::Reset_t>( DirectX::Hooked_Reset, 16 );
		DirectX::g_fnOriginalEndScene = d3d_device9->HookMethod<DirectX::EndScene_t>( DirectX::Hooked_EndScene, 42 );
		DM::g_fnOriginalDrawModel = studio_render->HookMethod<DM::DrawModel_t>( DM::Hooked_DrawModel, 29 );

		//Update hooks
		FSN::g_fnOriginalFrameStageNotify = client->HookMethod<FSN::FrameStageNotify_t>( FSN::Hooked_FrameStageNotify, 36 );

		//Move hooks
		CHLCM::g_fnOriginalCreateMoveCHL = client->HookMethod<CHLCM::CreateMoveCHL_t>( HookedCreateMoveNaked, 21 );

		//RenderView hook -> Can't hook this, it crashes even if correct meh
		SceneEnd::g_fnOriginalSceneEnd = render_view->HookMethod<SceneEnd::SceneEnd_t>( SceneEnd::Hooked_SceneEnd, 9 );

		//OverrideView hooks
		OView::g_fnOriginalOverrideView = client_mode->HookMethod<OView::OverrideView_t>( OView::Hooked_OverrideView, 18 );

		//Interpolate Fix Hook
		InterpFix::g_fnOriginalPlayingTimeDemo = engine->HookMethod<InterpFix::IsPlayingTimeDemo_t>( InterpFix::HookedIsPlayingTimeDemo, 84 );

		// Begin frame machine working/broke cbf checking lol
		BeginFrame::g_fnOriginalBeginFrame = studio_render->HookMethod<BeginFrame::BeginFrame_t>( BeginFrame::Hooked_BeginFrame, 9 );

		//-----------------------------------------------------------------------------------------------

	//	proxies::apply_proxy();

		//------------------------------------------------------------------------------------------------

		//Setup convar
		SSDK::ConVar *matprocess_enable = SSDK::I::CVar( )->FindVar( XorStr( "mat_postprocess_enable" ) );
		SSDK::SpoofedConvar *matprocess_enable_spoofed = new SSDK::SpoofedConvar( matprocess_enable );
		matprocess_enable_spoofed->SetInt( 0 );

		SSDK::ConVar *interpolate = SSDK::I::CVar( )->FindVar( XorStr( "cl_interpolate" ) );
		SSDK::SpoofedConvar *interpolate_spoofed = new SSDK::SpoofedConvar( interpolate );
		interpolate_spoofed->SetInt( 0 );

		// hell yeah bro untouch

		//------------------------------------------------------------------------------------------------

		// REGISTER LISTENERS
		CHurtListener::singleton( )->init( );
		CRSListener::singleton( )->init( );
		CFireListener::singleton( )->init( );
		CBulletListener::singleton( )->init( );
		// LISTENERS REGISTERED

		// do u really think, i'm going to ruin my fucking config, to look like I didn't base my cheat off another? u thought wrong u silly goose
		std::ifstream ifs( "hackerhook.config", std::ios::binary );
		ifs.read( ( char* ) &OptionsManager, sizeof( OptionsManager ) );
	}

	void RestoreAllTable( ) {
		client->Unhook( );
		model_render->Unhook( );
		vgui->Unhook( );
		client_mode->Unhook( );
		d3d_device9->Unhook( );
		engine->Unhook( );

		SetWindowLongPtr( DirectX::g_hWindow, GWLP_WNDPROC, ( LONG_PTR ) DirectX::g_pOldWindowProc );
	}
}