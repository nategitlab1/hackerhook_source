#include "Interfaces.h"

// see i was going to just re-do the sdk but this sexy mf idk if he wants a shoutout but i will if he asks when he wakes up, gave me new base. so fuck u.

// oh also, new way of getting interfaces no more bullshit.

namespace SSDK {

	/* ~~ ~~ */

	IVEngineClient *I::m_pEngine = nullptr;
	IClientEntityList *I::m_pEntityList = nullptr;
	ICvar *I::m_pCvar = nullptr;
	IBaseClientDLL *I::m_pClient = nullptr;
	IVModelRender *I::m_pModelRender = nullptr;
	IVModelInfo *I::m_pModelInfo = nullptr;
	IMaterialSystem *I::m_pMatSystem = nullptr;
	IPanel *I::m_pVGuiPanel = nullptr;
	ISurface *I::m_pVGuiSurface = nullptr;
	IVDebugOverlay *I::m_pDebugOverlay = nullptr;
	IClientMode *I::m_pClientMode = nullptr;
	IEngineTrace *I::m_pEngineTrace = nullptr;
	IVRenderView *I::m_pRenderView = nullptr;
	IMDLCache *I::m_pMDLCache = nullptr;
	IPhysicsSurfaceProps *I::m_pPhysicsProps = nullptr;
	IGlobalVarsBase *I::m_pGlobalVars = nullptr;
	IGameMovement *I::m_pGameMovement = nullptr;
	CPrediction *I::m_pPrediction = nullptr;
	IInput *I::m_pInput = nullptr;
	IViewRender *I::m_pViewRender = nullptr;
	IStudioRender *I::m_pStudioRender = nullptr;
	C_TEFireBullets *I::m_pTE_FireBullets = nullptr;
	IGameEventManager2 *I::m_pGameEvents = nullptr;

	/* ~~ ~~ */

	IVEngineClient* I::Engine( ) {
		if ( !m_pEngine )
			m_pEngine = get_interface<IVEngineClient*>( XorStr( "engine.dll" ), XorStr( "VEngineClient" ) );

		return m_pEngine;
	}

	IClientEntityList* I::EntityList( ) {
		if ( !m_pEntityList )
			m_pEntityList = get_interface<IClientEntityList*>( XorStr( "client.dll" ), XorStr( "VClientEntityList" ) );

		return m_pEntityList;
	}

	ICvar* I::CVar( ) {
		if ( !m_pCvar )
			m_pCvar = get_interface<ICvar*>( XorStr( "vstdlib.dll" ), XorStr( "VEngineCvar" ) );

		return m_pCvar;
	}

	IBaseClientDLL* I::Client( ) {
		if ( !m_pClient )
			m_pClient = get_interface<IBaseClientDLL*>( XorStr( "client.dll" ), XorStr( "VClient" ) );

		return m_pClient;
	}

	IVModelRender* I::ModelRender( ) {
		if ( !m_pModelRender )
			m_pModelRender = get_interface<IVModelRender*>( XorStr( "engine.dll" ), XorStr( "VEngineModel" ) );

		return m_pModelRender;
	}

	IVModelInfo* I::ModelInfo( ) {
		if ( !m_pModelInfo )
			m_pModelInfo = get_interface<IVModelInfo*>( XorStr( "engine.dll" ), XorStr( "VModelInfoClient" ) );

		return m_pModelInfo;
	}

	IMaterialSystem* I::MatSystem( ) {
		if ( !m_pMatSystem )
			m_pMatSystem = get_interface<IMaterialSystem*>( XorStr( "materialsystem.dll" ), XorStr( "VMaterialSystem" ) );

		return m_pMatSystem;
	}

	IPanel* I::VGUIPanel( ) {
		if ( !m_pVGuiPanel )
			m_pVGuiPanel = get_interface<IPanel*>( XorStr( "vgui2.dll" ), XorStr( "VGUI_Panel" ) );

		return m_pVGuiPanel;
	}

	ISurface* I::MatSurface( ) {
		if ( !m_pVGuiSurface )
			m_pVGuiSurface = get_interface<ISurface*>( XorStr( "vguimatsurface.dll" ), XorStr( "VGUI_Surface" ) );

		return m_pVGuiSurface;
	}

	IVDebugOverlay* I::DebugOverlay( ) {
		if ( !m_pDebugOverlay )
			m_pDebugOverlay = get_interface<IVDebugOverlay*>( XorStr( "engine.dll" ), XorStr( "VDebugOverlay" ) );
		return m_pDebugOverlay;
	}

	IClientMode* I::ClientMode( ) {
		if ( !m_pClientMode ) {
			m_pClientMode = **( IClientMode*** ) ( ( *( DWORD** ) I::Client( ) )[ 10 ] + 0x5 );
		}
		return m_pClientMode;
	}

	IEngineTrace* I::EngineTrace( ) {
		if ( !m_pEngineTrace )
			m_pEngineTrace = get_interface<IEngineTrace*>( XorStr( "engine.dll" ), XorStr( "EngineTraceClient" ) );

		return m_pEngineTrace;
	}

	IVRenderView *I::RenderView( ) {
		if ( !m_pRenderView )
			m_pRenderView = get_interface<IVRenderView*>( XorStr( "engine.dll" ), XorStr( "VEngineRenderView" ) );

		return m_pRenderView;
	}

	IMDLCache* I::MDLCache( ) {
		if ( !m_pMDLCache )
			m_pMDLCache = get_interface<IMDLCache*>( XorStr( "datacache.dll" ), XorStr( "MDLCache" ) );

		return m_pMDLCache;
	}

	IPhysicsSurfaceProps* I::PhysicsProps( ) {
		if ( !m_pPhysicsProps )
			m_pPhysicsProps = get_interface<IPhysicsSurfaceProps*>( XorStr( "vphysics.dll" ), XorStr( "VPhysicsSurfaceProps" ) );

		return m_pPhysicsProps;
	}

	IGlobalVarsBase* I::GlobalVars( ) {
		if ( !m_pGlobalVars )
			m_pGlobalVars = **( IGlobalVarsBase*** ) ( ( *( DWORD** ) I::Client( ) )[ 0 ] + 0x1B );

		return m_pGlobalVars;
	}

	IGameMovement* I::GameMovement( ) {
		if ( !m_pGameMovement )
			m_pGameMovement = get_interface<IGameMovement*>( XorStr( "client.dll" ), XorStr( "GameMovement" ) );

		return m_pGameMovement;
	}

	CPrediction* I::Prediction( ) {
		if ( !m_pPrediction )
			m_pPrediction = get_interface<CPrediction*>( XorStr( "client.dll" ), XorStr( "VClientPrediction" ) );

		return m_pPrediction;
	}

	IInput* I::Input( ) {
		if ( !m_pInput )
			m_pInput = *( IInput** ) ( ( *( DWORD** ) Client( ) )[ 15 ] + 0x1 );

		return m_pInput;
	}

	IViewRender* I::ViewRender( ) {
		if ( !m_pViewRender )
			m_pViewRender = **( IViewRender*** ) ( ( *( uintptr_t** ) Client( ) )[ 27 ] + 0x8 );

		return m_pViewRender;
	}

	IStudioRender* I::StudioRender( ) {
		if ( !m_pStudioRender )
			m_pStudioRender = get_interface<IStudioRender*>( XorStr( "studiorender.dll" ), XorStr( "VStudioRender" ) );

		return m_pStudioRender;
	}

	IGameEventManager2* I::GameEvents( ) {
		if ( !m_pGameEvents )
			m_pGameEvents = get_interface<IGameEventManager2*>( XorStr( "engine.dll" ), XorStr( "GAMEEVENTSMANAGER002" ) );

		return m_pGameEvents;
	}
}