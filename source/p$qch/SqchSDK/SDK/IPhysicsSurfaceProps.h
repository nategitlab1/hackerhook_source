#pragma once

#include "..\SDKHelper\Utilities.h"

namespace SSDK {
	struct surfacephysicsparams_t
	{
		float friction;
		float elasticity;
		float density;
		float thickness;
		float dampening;
	};

	struct surfaceaudioparams_t
	{
		float audioReflectivity;
		float audioHardnessFactor;
		float audioRoughnessFactor;
		float scrapeRoughThreshold;
		float impactHardThreshold;
		float audioHardMinVelocity;
		float highPitchOcclusion;
		float midPitchOcclusion;
		float lowPitchOcclusion;
	};

	struct surfacesoundnames_t
	{
		short walkLeft;
		short walkRight;
		short runLeft;
		short runRight;
		short impactsoft;
		short impacthard;
		short scrapesmooth;
		short scraperough;
		short bulletimpact;
		short rolling;
		short breakSound; // named "break" in vphysics.dll but since break is also a type rename it to breakSound
		short strain;
	};

	struct surfacegameprops_t
	{
		float maxspeedfactor;
		float jumpfactor;
		float penetrationmodifier;
		float damagemodifier;
		uint16_t material;
		uint8_t climbable;
	};

	struct surfacedata_t
	{
		surfacephysicsparams_t physics;
		surfaceaudioparams_t audio;
		surfacesoundnames_t sounds;
		surfacegameprops_t game;
		char pad[48];
	}; // size = 0x94

	class IPhysicsSurfaceProps {
	public:
		surfacedata_t* GetSurfaceData(int surfaceDataIndex) {
			typedef surfacedata_t*(__thiscall *OrigFn)(void*, int);
			return CallVFunction<OrigFn>(this, 5)(this, surfaceDataIndex);
		}
	};
}