#pragma once

namespace SSDK {
	class PlayerInfo {
	public:
		char           pad_0x0000[0x10]; //0x0000
		char           szName[32]; //0x0010 
		char           pad_0x0030[0x64]; //0x0030
		char           szSteamID[20]; //0x0094 SteamID on text format (STEAM_X:Y:Z)
		char           pad_0x00A8[0x10]; //0x00A8
		unsigned long  iSteamID; //0x00B8 
		char           unknown2[0x14C];
	};

	class AnimationLayer
	{
	public:
		char  pad_0000[20];
		uint32_t m_nOrder; //0x0014
		uint32_t m_nSequence; //0x0018
		float_t m_flPrevCycle; //0x001C
		float_t m_flWeight; //0x0020
		float_t m_flWeightDeltaRate; //0x0024
		float_t m_flPlaybackRate; //0x0028
		float_t m_flCycle; //0x002C
		void *m_pOwner; //0x0030 // player's thisptr
		char  pad_0038[4]; //0x0034
		unsigned char m_bInHitGroundAnimation; //0x109
		float m_flHeadHeightOrOffsetFromHittingGroundAnimation; //0x118 from 0 to 1, is 1 when standing
	}; //Size: 0x0038
}