#include "Offsets.h"

namespace SSDK {
	CGlowObjectManager *O::m_pGlowManager = nullptr;
	IMoveHelper *O::m_pMoveHelper = nullptr;
	IViewRender *O::m_pViewRender = nullptr;

	CGlowObjectManager* O::GlowManager() {
		if (!m_pGlowManager) {
			m_pGlowManager = (CGlowObjectManager*)(*(DWORD*)(FindSignature(XorStr("client.dll"), XorStr("A1 ? ? ? ? A8 01 75 4B")) + 0x1)) + 4;
		//	m_pGlowManager = (CGlowObjectManager*)(*(DWORD*)(FindSignature(XorStr("client.dll"), XorStr("0F 11 05 ?? ?? ?? ?? 83 C8 01 C7 05 ?? ?? ?? ?? 00 00 00 00")) + 3));
		}
		return m_pGlowManager;
	}

	IMoveHelper* O::MoveHelper() {
		if (!m_pMoveHelper) {
			m_pMoveHelper = **(IMoveHelper***)(FindSignature(XorStr("client.dll"), XorStr("8B 0D ? ? ? ? 8B 46 08 68")) + 0x2);
		}
		return m_pMoveHelper;
	}

	IViewRender* O::ViewRender() {
		if (!m_pViewRender) {
			m_pViewRender = *(IViewRender**)(FindSignature(XorStr("client.dll"), XorStr("A1 ? ? ? ? B9 ? ? ? ? C7 05 ? ? ? ? ? ? ? ? FF 10")) + 1);
		}
		return m_pViewRender;
	}
}