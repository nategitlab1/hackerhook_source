#include "CResolver.h"

#define TICK_INTERVAL			( SSDK::I::GlobalVars()->interval_per_tick )
#define TIME_TO_TICKS( dt )		( (int)( 0.5f + (float)(dt) / TICK_INTERVAL ) )
#define TICKS_TO_TIME( t )		( TICK_INTERVAL *( t ) )

bool triggers_activity( CBaseEntity* entity, int activity )
{
	int seq_activity[ 64 ];

	// for loop for all 13 anim layers.
	for ( int j = 0; j < 13; j++ )
	{
		// self explanatory >.<
		seq_activity[ entity->EntIndex( ) ] = entity->GetSequenceActivity( entity->GetAnimOverlay( j ).m_nSequence ); /* backtracking::tick_record contains anim layers, maybe use that? */

		// also self explanatory <.>
		if ( seq_activity[ entity->EntIndex( ) ] == activity )
		{
			return true;
		}
	}

	return false;
}

float_t CResolver::ld(CBaseEntity* ent, int i) {
	float finala = 0;
	for (int x = 0; x < i; x++) {
		finala += eyes[ent->EntIndex()][0] - lbys[ent->EntIndex()][0];
	}
	return finala / i;
}

int CResolver::GetDifferentDeltas(CBaseEntity* ent) {
	auto asdf = [](float yaw) {
		while (yaw > 180.f)
			yaw -= 360.f;
		while (yaw < -180.f)
			yaw += 360.f;
		return yaw;
	};
	std::vector<float> vec;
	for (int i = 0; i < 8; i++) {
		float curdelta = ld(ent, i);
		bool add = true;
		for (auto fl : vec) {
			if (!(abs(asdf(curdelta - fl)) > 10.f)) {
				add = false;
			}
		}
		if (add) {
			vec.push_back(curdelta);
		}
	}
	return vec.size();
}

float CResolver::GetDeltaByComparingEightTicks(CBaseEntity* ent) {
	int modulo = 1;
	int difangles = GetDifferentDeltas(ent);
	int inc = modulo * difangles;

	return ld(ent, difangles);

}

float CResolver::GetMostFrequent(int num)
{
	int count = 1;
	int currentIndex = 0;
	for (int i = 1; i < 8; i++)
	{
		if (lbys[num][i] == lbys[num][currentIndex])
			count++;
		else
			count--;
		if (count == 0)
		{
			currentIndex = i;
			count = 1;
		}
	}
	float mostFreq = lbys[num][currentIndex];
	return mostFreq;
}

void CResolver::SetLBYs(int num, CBaseEntity* ent) {
	for (int i = 0; i < 8; i++)
	{
		if (i != 0)
			lbys[num][i + 1] = lbys[num][i]; //push all values backwards in the array
		else
			lbys[num][i] = *ent->GetLowerBodyYaw(); //write the now nulled first value to be equal to the entity lby
	}
}

void CResolver::SetMLBYs(int num, CBaseEntity* ent) {
	for (int i = 0; i < 8; i++)
	{
		if (i != 0)
			lastmoving[num][i + 1] = lastmoving[num][i]; //push all values backwards in the array
		else
			lastmoving[num][i] = *ent->GetLowerBodyYaw(); //write the now nulled first value to be equal to the entity lby
	}
}

void CResolver::SetEyes(int num, CBaseEntity* ent) {
	for (int i = 0; i < 8; i++)
	{
		if (i != 0)
			eyes[num][i + 1] = eyes[num][i]; //push all values backwards in the array
		else
			eyes[num][i] = ent->GetEyeAngles()->y; //write the now nulled first value to be equal to the entity lby
	}
}

bool CResolver::IsFakewalking(CBaseEntity *player)
{
		bool bFakewalking = false,
		stage1 = false,            // stages needed cause we are iterating all layers, eitherwise won't work :)
		stage2 = false,
		stage3 = false;
		SSDK::AnimationLayer anim[13];



	for (int i = 0; i < 13; i++)
	{
		if (anim[i].m_nSequence == 26 && anim[i].m_flWeight < 0.4f)
			stage1 = true;
		if (anim[i].m_nSequence == 7 && anim[i].m_flWeight > 0.001f)
			stage2 = true;
		if (anim[i].m_nSequence == 2 && anim[i].m_flWeight == 0)
			stage3 = true;
	}

	if (stage1 && stage2)
		if (stage3 || (*player->GetFlags() & (int)SSDK::EntityFlags::FL_DUCKING)) // since weight from stage3 can be 0 aswell when crouching, we need this kind of check, cause you can fakewalk while crouching, thats why it's nested under stage1 and stage2
			bFakewalking = true;
		else
			bFakewalking = false;
	else
		bFakewalking = false;

	return bFakewalking;
}

void CResolver::Resolver_Fake_walk(CBaseEntity *entity) {

	// get localplayer ptr
	auto local = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(SSDK::I::Engine()->GetLocalPlayer());

	if (!entity->isValidPlayer())
		return;

	auto lowerbody = entity->GetLowerBodyYaw();

	float lby_left = *lowerbody + 70;
	float lby_right = *lowerbody - 70;
	float lby_inverse = *lowerbody + 180;
	float lby_back = *lowerbody - 180;
	float lby_half_left = *lowerbody + 45;
	float lby_half_right = *lowerbody - 45;

	if (Globals::missed_shots == 0)
	{
		entity->GetEyeAngles()->y = GetMostFrequent(entity->EntIndex()); // set angles to most common lby

	}
	else { //brute this bitch
		switch (Globals::missed_shots % 5)
		{
		case 0: 
			entity->GetEyeAngles()->y = lby_back; 
			break;
		case 1: 
			entity->GetEyeAngles()->y = lby_inverse;
			break;
		case 2: 
			entity->GetEyeAngles()->y = lby_left; 
			break;
		case 3: 
			entity->GetEyeAngles()->y = lby_right; 
			break;
		case 4: 
			entity->GetEyeAngles()->y = lby_half_left; 
			break;
		case 5: 
			entity->GetEyeAngles()->y = lby_half_right;
			break;
		}
	}
}

void CResolver::Resolver_standing(CBaseEntity *entity) {

	// get localplayer ptr
	auto local = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(SSDK::I::Engine()->GetLocalPlayer());

	if (!entity->isValidPlayer())
		return;

	auto lowerbody = entity->GetLowerBodyYaw();

	float lby_left = *lowerbody + 70;
	float lby_right = *lowerbody - 70;
	float lby_inverse = *lowerbody + 180;
	float lby_back = *lowerbody - 180;
	float lby_half_left = *lowerbody + 45;
	float lby_half_right = *lowerbody - 45;
	float lby_90 = *lowerbody + 90;

	if (Globals::missed_shots == 0) {
		entity->GetEyeAngles()->y = lastmoving[entity->EntIndex()][0]; // set angles to last moving lby if missed < one shot
	}
	else if (Globals::missed_shots == 2)
	{
		entity->GetEyeAngles()->y = GetDeltaByComparingEightTicks(entity); //GetMostFrequent(entity->EntIndex()); // set angles to most common lby
	}
	else { //brute this bitch
		switch (Globals::missed_shots % 6)
		{
		case 0:
			entity->GetEyeAngles()->y = lby_back;
			break;
		case 1:
			entity->GetEyeAngles()->y = lby_inverse;
			break;
		case 2:
			entity->GetEyeAngles()->y = lby_left;
			break;
		case 3:
			entity->GetEyeAngles()->y = lby_right;
			break;
		case 4:
			entity->GetEyeAngles()->y = lby_half_left;
			break;
		case 5:
			entity->GetEyeAngles()->y = lby_half_right;
			break;
		case 6:
			entity->GetEyeAngles()->y = lby_90;
			break;
		}
	}
}

void CResolver::Solve_Yaw( )
{
	// get localplayer ptr
	auto local = ( CBaseEntity* ) SSDK::I::EntityList( )->GetClientEntity( SSDK::I::Engine( )->GetLocalPlayer( ) );

	// ptr entity
	CBaseEntity* entity;

	for (int i = 1; i <= SSDK::I::GlobalVars()->maxClients; i++)
	{
		entity = (CBaseEntity*)SSDK::I::EntityList()->GetClientEntity(i);

		if (!entity->isValidPlayer())
			continue;

		auto moving = (entity->GetVelocity()->Length2D() > 0.5);
		auto on_ground = (*entity->GetFlags() & (int)SSDK::EntityFlags::FL_ONGROUND);
		auto eyeangles = entity->GetEyeAngles()->y;
		auto lowerbody = entity->GetLowerBodyYaw();

		SetLBYs(entity->EntIndex(), entity);
		SetEyes(entity->EntIndex(), entity);

		// moving and on ground
		if (moving && on_ground)
		{
			// moving resolver
			entity->GetEyeAngles()->y = *entity->GetLowerBodyYaw();

			//moving storage
			SetMLBYs(entity->EntIndex(), entity);

		}

		// they are not moving and are on ground.
		else if (!moving && on_ground)
		{
			Resolver_standing(entity);
		}
		else if (IsFakewalking(entity))
		{
			// fakewalk check
			Resolver_Fake_walk(entity);
		}
	}
}