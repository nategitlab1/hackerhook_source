#include "FSN.h"
#include "Globals.h"
#include "CGlowObjectManager.h"

#define	HITGROUP_GENERIC	0
#define	HITGROUP_HEAD		1
#define	HITGROUP_CHEST		2
#define	HITGROUP_STOMACH	3
#define HITGROUP_LEFTARM	4	
#define HITGROUP_RIGHTARM	5
#define HITGROUP_LEFTLEG	6
#define HITGROUP_RIGHTLEG	7
#define HITGROUP_GEAR		10

#define TICK_INTERVAL			( SSDK::I::GlobalVars()->interval_per_tick )
#define TIME_TO_TICKS( dt )		( (int)( 0.5f + (float)(dt) / TICK_INTERVAL ) )
#define TICKS_TO_TIME( t )		( SSDK::I::GlobalVars()->interval_per_tick *( t ) )

namespace FSN {

	FrameStageNotify_t g_fnOriginalFrameStageNotify;
	VOID __fastcall Hooked_FrameStageNotify( void* thisptr, void* edx, SSDK::ClientFrameStage_t curStage ) {
		CBaseEntity *local = CBaseEntity::GetLocalPlayer( );

		SSDK::Vector vecAimPunch;
		SSDK::Vector vecViewPunch;

		SSDK::Vector* pAimPunch = nullptr;
		SSDK::Vector* pViewPunch = nullptr;

		static bool DoThirdperson = false;
		std::unique_ptr<CResolver> g_pResolver = std::make_unique<CResolver>( );


		if ( SSDK::I::Engine( )->isInGame( ) ) {
			if ( curStage == SSDK::ClientFrameStage_t::FRAME_NET_UPDATE_POSTDATAUPDATE_START ) {

				// not good, use a proxy and actually do it proper or do pazzo animation update bullshit, this doesn't predict well, also i think u should do in cm.
				static float next_update_time;
				if ( local->IsAlive( ) )
				{
					float tick = Globals::tick_base;
					float delta = std::abs( Utilities::Normalize( Globals::real_angles.y - *local->GetLowerBodyYaw( ) ) );

					Globals::body_updated = false;

					bool moving = false;

					if ( next_update_time - tick > 1.1 )
						next_update_time = 0;

					if ( local->GetVelocity( )->Length2D( ) > 0.1 )
						moving = true;

					if ( local->GetVelocity( )->Length2D( ) == 0 && !moving )
					{
						if ( tick > next_update_time + 1.1f )
						{
							next_update_time = tick + 1.1f;
							Globals::body_updated = true;
						}
					}
					else if ( local->GetVelocity( )->Length2D( ) == 0 && moving )
					{
						if ( tick > next_update_time + 0.22f )
						{
							next_update_time = tick + 1.1f;
							Globals::body_updated = true;
							moving = false;
						}
					}
				}

				// end of breaker meme

				for ( int i = 1; i < SSDK::I::Engine( )->GetMaxClients( ); ++i ) {

					CBaseEntity *target = ( CBaseEntity* ) SSDK::I::EntityList( )->GetClientEntity( i );

					if ( target->isValidPlayer( ) )
					{
						Globals::missed_shots = Globals::shots_fired - Globals::hit_shots;
					}
				}

				if ( OptionsManager.bCorrect )
				{
					g_pResolver->Solve_Yaw( );
				}
			}

			if ( curStage == SSDK::ClientFrameStage_t::FRAME_RENDER_START ) {

				for ( int i = 1; i <= SSDK::I::GlobalVars( )->maxClients; i++ )
				{
					if ( i == SSDK::I::Engine( )->GetLocalPlayer( ) )
						continue;

					auto entity = SSDK::I::EntityList( )->GetClientEntity( i );

					if ( !entity )
						continue;

					*( int* ) ( ( uintptr_t ) entity + 0xA30 ) = SSDK::I::GlobalVars( )->framecount; // we'll skip occlusion checks now
					*( int* ) ( ( uintptr_t ) entity + 0xA28 ) = 0; // clear occlusion flags
				}

				if ( ( OptionsManager.iThirdPerson && DoThirdperson )/* || OptionsManager.bNoSmoke*/ )
				{
					static bool spoofed = false;
					if ( !spoofed )
					{
						SSDK::ConVar* sv_cheats = SSDK::I::CVar( )->FindVar( "sv_cheats" );
						SSDK::SpoofedConvar* sv_cheats_s = new SSDK::SpoofedConvar( sv_cheats );
						sv_cheats_s->SetInt( 1 );
						spoofed = true;
					}
				}

				static SSDK::QAngle vecAngles;
				SSDK::I::Engine( )->GetViewAngles( vecAngles );

				if ( OptionsManager.iThirdPerson && local->IsAlive( ) && DoThirdperson )
				{
					if ( !SSDK::I::Input( )->m_fCameraInThirdPerson )
					{
						SSDK::I::Input( )->m_fCameraInThirdPerson = true;
						SSDK::I::Input( )->m_vecCameraOffset = SSDK::Vector( vecAngles.x, vecAngles.y, 141 );
					}
				}
				else
				{
					SSDK::I::Input( )->m_fCameraInThirdPerson = false;
					SSDK::I::Input( )->m_vecCameraOffset = SSDK::Vector( vecAngles.x, vecAngles.y, 0 );
				}

				/* 0x58 == x */

				static float pressTime;
				if ( GetAsyncKeyState(0x58) && abs( pressTime - SSDK::I::GlobalVars( )->curtime ) > 0.5 )
				{
					pressTime = SSDK::I::GlobalVars( )->curtime;
					DoThirdperson = !DoThirdperson;
				}

				if ( OptionsManager.bNoVisRecoil ) {
					pAimPunch = local->AimPunch( );
					pViewPunch = local->ViewPunch( );

					vecAimPunch = *pAimPunch;
					vecViewPunch = *pViewPunch;

					*pAimPunch = SSDK::QAngle( 0, 0, 0 );
					*pViewPunch = SSDK::QAngle( 0, 0, 0 );
				}

				if ( OptionsManager.bClantagchanger ) {

					static std::string cur_clantag = "hackerhook ";
					static int old_time;

					static int i = 0;

					if ( SSDK::I::Engine( )->isInGame( ) ) {
						if ( i > 32 )
						{
							Utilities::marquee( cur_clantag );
							Utilities::SetClanTag( cur_clantag.c_str( ), cur_clantag.c_str( ) );
							i = 0;
						}
						else
						{
							i++;
						}
					}
				}


				if ( OptionsManager.bNoFlash )
					if ( *local->flashDuration( ) > 0.f )
						*local->flashDuration( ) = 0.f;

				//	Nightmode();

					//THIRDPERSON AA
				if ( *( bool* ) ( ( DWORD ) SSDK::I::Input( ) + 0xA5 ) )
					*( SSDK::QAngle* )( ( DWORD ) local + 0x031C8 ) = SSDK::QAngle( CreateMoveAngles.x, CreateMoveAngles.y, 0 );//OptionsManager.qaLocalEyesAngle;
			}
		}

		//Original
		g_fnOriginalFrameStageNotify( thisptr, curStage );

		if ( OptionsManager.bNoVisRecoil ) {
			if ( curStage == SSDK::ClientFrameStage_t::FRAME_RENDER_START ) {
				if ( pAimPunch && pViewPunch ) {
					*pAimPunch = vecAimPunch;
					*pViewPunch = vecViewPunch;
				}
			}
		}

		if ( curStage == SSDK::ClientFrameStage_t::FRAME_RENDER_END ) {
			//UpdateClientAnim
			for ( int i = 1; i < SSDK::I::Engine( )->GetMaxClients( ); ++i ) {
				CBaseEntity *pClient = ( CBaseEntity* ) SSDK::I::EntityList( )->GetClientEntity( i );
				if ( pClient->isValidPlayer( ) ) {
					pClient->UpdateClientSideAnimation( );
				}
			}
		}
	}
}