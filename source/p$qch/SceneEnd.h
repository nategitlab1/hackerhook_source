#pragma once

#include "SqchSDK\Interfaces.h"
#include "EntityStructs.h"
#include "Globals.h"
#include "OptionsManager.h"

namespace SceneEnd
{
	typedef void(__thiscall* SceneEnd_t)(SSDK::IVRenderView*);

	extern SceneEnd_t g_fnOriginalSceneEnd;

	void __stdcall Hooked_SceneEnd();
}